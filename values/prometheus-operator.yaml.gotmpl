prometheus:
  prometheusSpec:
    scrapeInterval: "10s"
    storageSpec:
      volumeClaimTemplate:
        spec:
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: 10Gi
  ingress:
    enabled: false

kubeControllerManager:
  enabled: false

coreDns:
  enabled: false

kubeDns:
  enabled: true

kubeEtcd:
  enabled: false

kubeScheduler:
  enabled: false

# Disabled because on GKE kube-proxy listen only localhost
kubeProxy:
  enabled: false

grafana:
  enabled: true
  defaultDashboardsEnabled: true
  env:
    GF_AUTH_DISABLE_LOGIN_FORM: "true"
    GF_AUTH_DISABLE_SIGNOUT_MENU: "true"
    GF_AUTH_ANONYMOUS_ENABLED: "true"
    GF_AUTH_ANONYMOUS_ORG_ROLE: "Admin"
  ingress:
    enabled: false

  resources:
    limits:
      cpu: 20m
      memory: 256Mi
    requests:
      cpu: 10m
      memory: 128Mi

  sidecar:
    dashboards:
      enabled: true
      searchNamespace: ALL
    resources:
      limits:
        cpu: 20m
        memory: 256Mi
      requests:
        cpu: 10m
        memory: 16Mi

  additionalDataSources:
  - name: Loki
    type: loki
    orgId: 1
    access: proxy
    editable: true
    url: http://loki:3100/
    jsonData:
      maxLines: 1000

alertmanager:
  ingress:
    enabled: false

  alertmanagerSpec:
    replicas: 3
  resources:
    limits:
      cpu: 20m
      memory: 64Mi
    requests:
      cpu: 10m
      memory: 32Mi
  config:
    global:
      resolve_timeout: 5m
    route:
      group_by: ['alertname', 'job']
      group_wait: 30s
      group_interval: 1m
      repeat_interval: 12h
      receiver: 'telegram'
      routes:
      - match:
          severity: warning
        receiver: 'telegram'
      - match:
          severity: high
        receiver: 'telegram'
      - match:
          severity: critical
        receiver: 'telegram'
      - match:
          alertname: Watchdog
        receiver: 'null'
    receivers:
    - name: 'telegram'
      webhook_configs:
      - send_resolved: True
        url: http://prometheus-telegram-bot:9087/alert/{{ requiredEnv "TELEGRAM_ROOM_ID" }}
    - name: 'null' 

defaultRules:
  create: true
  rules:
    kubernetesResources: false

prometheusOperator:
  createCustomResource: false